<?php

namespace Supermetrics;

require_once('src/Repository/APIRepository.php');
require_once('src/Data/RegisterData.php');
require_once('src/Data/AuthData.php');
require_once('src/Config/APIConfig.php');
require_once('src/Controller/APIController.php');

use Supermetrics\Data\AuthData;
use Supermetrics\Data\RegisterData;

use Supermetrics\Repository\APIRepository;

use Supermetrics\Config\APIConfig;

use Supermetrics\Controller\APIController;

$configuration = new APIConfig();

$apiRepository = new APIRepository($configuration);

$registerData = new RegisterData(   $configuration->get('client_id'), 
                                    $configuration->get('email'), 
                                    $configuration->get('name'));

$apiController = new APIController($configuration, $apiRepository, $registerData);

if ($apiController->isAuthenticated()) {
    $apiController->fetchData();

    echo "AverageCharacterLengthPerMonth \n";

    echo $apiController->getAverageCharacterLengthPerMonth();

    echo "\nLongestPostByCharacterLengthPerMonth \n";

    echo $apiController->getLongestPostByCharacterLengthPerMonth();

    echo "\nTotalPostsSplitByWeek \n";

    echo $apiController->getTotalPostsSplitByWeek();

    echo "\nAverageNumberOfPostsPerUserPerMonth \n";

    echo $apiController->getAverageNumberOfPostsPerUserPerMonth();

    echo "\n";
}

?>