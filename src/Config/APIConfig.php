<?php

namespace Supermetrics\Config;

class APIConfig {

    private $configuration;

    function __construct() {

        /**
         * this data should not be in the code.
         * It should be kept as config in kubernetes
         * or can be kept as environmental variable.
         */
        $this->configuration = array(
            'baseURL' => "https://api.supermetrics.com",
            'registerURI' => "/assignment/register",
            'fetchDataURI' => "/assignment/posts",
            'client_id' => "ju16a6m81mhid5ue1z3v2g0uh",
            'email' => "ilkermoral@gmail.com",
            'name' => "ilker",
            'fetch_data_page_size' => 10

        );
    }

    function get($key) {
        return $this->configuration[$key];
    }

    function set($key, $value) {
        $this->configuration[$key] = $value;
    }
}

?>