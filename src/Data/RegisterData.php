<?php

namespace Supermetrics\Data;

class RegisterData {

    private $clientID;

    private $email;

    private $name;

    function __construct($clientID, $email, $name) {

        $this->clientID = $clientID;
        $this->email = $email;
        $this->name = $name;
    }

    function setClientID($clientID) {
        $this->clientID = $clientID;
    }

    function getClientID() {
        return $this->clientID;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function getEmail() {
        return $this->email;
    }

    function setName($name) {
        $this->name = $name;
    }

    function getName() {
        return $this->name;
    }
}

?>