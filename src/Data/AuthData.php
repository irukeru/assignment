<?php

namespace Supermetrics\Data;

class AuthData {

    const ONE_HOUR_IN_SECONDS = 3600;

    private $slToken;

    private $clientID;

    private $email;

    private $creationTime;

    function __construct($slToken, $clientID, $email, $creationTime) {

        $this->slToken = $slToken;
        $this->clientID = $clientID;
        $this->email = $email;
        $this->creationTime = $creationTime;
    }

    function setSlToken($slToken) {
        $this->slToken = $slToken;
    }

    function getSlToken() {
        return $this->slToken;
    }

    function setClientID($clientID) {
        $this->clientID = $clientID;
    }

    function getClientID() {
        return $this->clientID;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function getEmail() {
        return $this->email;
    }

    function setCreationTime($creationTime) {
        $this->creationTime = $creationTime;
    }

    function getCreationTime() {
        return $this->creationTime;
    }

    function isSlTokenValid() {
        return $this->creationTime + self::ONE_HOUR_IN_SECONDS > time();
    }
}
?>