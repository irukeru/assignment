<?php

namespace Supermetrics\Repository;

require_once('BaseRepository.php');
require_once( __DIR__.'/../Data/RegisterData.php' );
require_once( __DIR__.'/../Data/AuthData.php' );
require_once( __DIR__.'/../Config/APIConfig.php' );

use Supermetrics\Data\RegisterData;
use Supermetrics\Data\AuthData;
use Supermetrics\Config\APIConfig;
use Supermetrics\Repository\BaseRepository;

class APIRepository extends BaseRepository {

    private $config;

    function __construct($config) {
        parent::__construct($config->get('baseURL'));

        $this->config = $config;
    }

    function register($registerData) {

        $postParams = array(
            'client_id' => $registerData->getClientID(),
            'email' => $registerData->getEmail(),
            'name' => $registerData->getName()
        );

        $result = $this->post($postParams, $this->config->get('registerURI'));

        if ($result != null) {
            return new AuthData($result['data']['sl_token'], $result['data']['client_id'], $result['data']['email'], time());
        } else {
            return null;
        }


    }

    function fetchPosts($slToken, $page) {

        $getParams = array(
            'sl_token' => $slToken,
            'page' => $page
        );

        return $this->get($getParams, $this->config->get('fetchDataURI'));
    }
}

?>