<?php

namespace Supermetrics\Repository;

class BaseRepository {

    private $baseURL;

    function __construct($baseURL) {
        $this->baseURL = $baseURL;
    }

    function get($params, $uri, $header = null) {

        $curlConnection = curl_init();

        curl_setopt($curlConnection, CURLOPT_URL, $this->baseURL . $uri . '?' . http_build_query($params));
        curl_setopt($curlConnection, CURLOPT_RETURNTRANSFER, true);

        if ($header != null) {
            curl_setopt($curlConnection, CURLOPT_HTTPHEADER, $header);
        }

        $result = curl_exec($curlConnection);

        $status = $this->_checkCurlResponse($curlConnection, $uri);

        curl_close($curlConnection);

        if ($status) {
            return json_decode($result, true);
        }
        

        return null;
    }

    function post($params, $uri,  $header = null) {

        $curlConnection = curl_init();

        curl_setopt($curlConnection, CURLOPT_URL, $this->baseURL . $uri);
        curl_setopt($curlConnection, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($curlConnection, CURLOPT_POST, true);
        curl_setopt($curlConnection, CURLOPT_RETURNTRANSFER, true);

        if ($header != null) {
            curl_setopt($curlConnection, CURLOPT_HTTPHEADER, $header);
        }

        $result = curl_exec($curlConnection);

        $status = $this->_checkCurlResponse($curlConnection, $uri);

        curl_close($curlConnection);

        if ($status) {
            return json_decode($result, true);
        }

        return null;
    }

    private function _checkCurlResponse($curlConnection, $uri) {

        if (!curl_errno($curlConnection)) {
            $http_code = curl_getinfo($curlConnection, CURLINFO_HTTP_CODE);

            if ($http_code > 200 ) {
                echo 'Unexpected HTTP code: ', $http_code, "\n";
                echo $uri . "\n";
                return false;
            }

            return true;

        } else {
            echo "There has been an error occured! \n";
        }

        return false;
    }
}

?>