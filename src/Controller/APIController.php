<?php

namespace Supermetrics\Controller;

require_once(__DIR__.'/../Repository/APIRepository.php');
require_once(__DIR__.'/../Data/RegisterData.php');
require_once(__DIR__.'/../Data/AuthData.php');
require_once(__DIR__.'/../Config/APIConfig.php');

use Supermetrics\Data\AuthData;
use Supermetrics\Data\RegisterData;
use Supermetrics\Repository\APIRepository;
use Supermetrics\Config\APIConfig;

class APIController {

    private $configuration;

    private $apiRepository;

    private $registerData;

    private $authData;

    private $rawData;

    function __construct($configuration, $apiRepository, $registerData) {
        $this->configuration = $configuration;
        $this->apiRepository = $apiRepository;
        $this->registerData = $registerData;
        $this->rawData = array();

        $this->authenticate();
    }

    function authenticate() {
        $this->authData = $this->apiRepository->register($this->registerData);
    }

    function isAuthenticated() {
        return $this->authData != null ? $this->authData->isSlTokenValid() : false;
    }

    function fetchData() {
        for ($i = 1; $i <= $this->configuration->get('fetch_data_page_size'); $i++ ) {
            if (!$this->isAuthenticated()) {
                $this->authenticate();
            }
            $_fetchData = $this->apiRepository->fetchPosts($this->authData->getSlToken(), $i);
            if ($_fetchData != null) {
                $this->rawData = array_merge($this->rawData, $_fetchData['data']['posts']);
            }
        }
    }

    function getAverageCharacterLengthPerMonth() {

        $aggregatedData = [];
        $calculations = [];

        for ($i = 0; $i < count($this->rawData); $i++) {

            $year = date('Y', strtotime($this->rawData[$i]['created_time']));
            $month = date('m', strtotime($this->rawData[$i]['created_time']));

            $length = strlen($this->rawData[$i]['message']) - substr_count($this->rawData[$i]['message'], ' ');

            if (!isset($calculations[$year][$month])) {
                $calculations[$year][$month]["count"] = 0;
                $calculations[$year][$month]["total"] = 0;
            }
            
            $calculations[$year][$month]["count"]++;
            $calculations[$year][$month]["total"] += $length;
            
            $aggregatedData[$year][$month] = round(($calculations[$year][$month]["total"] / $calculations[$year][$month]["count"]), 2);
        }

        return json_encode($aggregatedData);
    }

    function getLongestPostByCharacterLengthPerMonth() {

        $aggregatedData = [];

        for ($i = 0; $i < count($this->rawData); $i++) {

            $year = date('Y', strtotime($this->rawData[$i]['created_time']));
            $month = date('m', strtotime($this->rawData[$i]['created_time']));

            $length = strlen($this->rawData[$i]['message']) - substr_count($this->rawData[$i]['message'], ' ');

            if (!isset($aggregatedData[$year][$month])) {
                $aggregatedData[$year][$month] = 0;
            }
            

            if ($length > $aggregatedData[$year][$month]) {
                $aggregatedData[$year][$month] = $length;
            }
        }

        return json_encode($aggregatedData);
    }

    function getTotalPostsSplitByWeek() {

        $aggregatedData = [];

        for ($i = 0; $i < count($this->rawData); $i++) {

            $year = date('Y', strtotime($this->rawData[$i]['created_time']));
            $week = date('W', strtotime($this->rawData[$i]['created_time']));

            if (!isset($aggregatedData[$year][$week])) {
                $aggregatedData[$year][$week] = 0;
            }

            $aggregatedData[$year][$week]++;
        }

        return json_encode($aggregatedData);
    }

    function getAverageNumberOfPostsPerUserPerMonth() {

        $aggregatedData = [];
        $calculations = [];

        for ($i = 0; $i < count($this->rawData); $i++) {

            $year = date('Y', strtotime($this->rawData[$i]['created_time']));
            $month = date('m', strtotime($this->rawData[$i]['created_time']));

            $userID = $this->rawData[$i]['from_id'];

            if (!isset($aggregatedData[$year][$month])) {
                $calculations[$year][$month]["total"] = 0;
                $aggregatedData[$year][$month] = 0;
            }

            $calculations[$year][$month]["userCount"][$userID] = true;
            $calculations[$year][$month]["total"]++; 

            $aggregatedData[$year][$month] = round(($calculations[$year][$month]["total"] / count($calculations[$year][$month]["userCount"])), 2);
        }

        return json_encode($aggregatedData);
    }
}

?>