<?php

require_once( __DIR__.'/../../src/Data/AuthData.php' );

use PHPUnit\Framework\TestCase;

use Supermetrics\Data\AuthData;

final class AuthDataTest extends TestCase {

    public function testCanBeCreatedFromValidData() {

        $fakeData = array(
            "slToken" => 'asdfghjk',
            "clientID" => '123456',
            "email" => 'asd@ghj.com',
            "time" => 3600
        );

        $authData = new AuthData($fakeData["slToken"], $fakeData["clientID"], $fakeData["email"], $fakeData["time"]);
    
        $this->assertEquals($fakeData['slToken'], $authData->getSlToken());
        $this->assertEquals($fakeData['clientID'], $authData->getClientID());
        $this->assertEquals($fakeData['email'], $authData->getEmail());
    }

    public function testIsSlTokenValidShouldReturnFalse() {

        $fakeData = array(
            "slToken" => 'asdfghjk',
            "clientID" => '123456',
            "email" => 'asd@ghj.com',
            "time" => 3600
        );

        $authData = new AuthData($fakeData["slToken"], $fakeData["clientID"], $fakeData["email"], $fakeData["time"]);

        $this->assertEquals(false, $authData->isSlTokenValid());
    }

    public function testIsSlTokenValidShouldReturnTrue() {

        $fakeData = array(
            "slToken" => 'asdfghjk',
            "clientID" => '123456',
            "email" => 'asd@ghj.com',
            "time" => time()
        );

        $authData = new AuthData($fakeData["slToken"], $fakeData["clientID"], $fakeData["email"], $fakeData["time"]);

        $this->assertEquals(true, $authData->isSlTokenValid());
    }
}

?>